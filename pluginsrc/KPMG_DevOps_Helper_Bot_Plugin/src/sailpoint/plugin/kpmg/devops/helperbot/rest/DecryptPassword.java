package sailpoint.plugin.kpmg.devops.helperbot.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sailpoint.api.SailPointContext;
import sailpoint.api.SailPointFactory;
import sailpoint.rest.plugin.BasePluginResource;
import sailpoint.rest.plugin.RequiredRight;
import sailpoint.tools.GeneralException;

@Path("KPMG/DevOps/HelperBot")
@RequiredRight("KPMG_DevOps_HelperBot_SPRight")
public class DecryptPassword extends BasePluginResource {
	public static final Logger log = Logger.getLogger(DecryptPassword.class);
	
	@SuppressWarnings("unchecked")
	@GET
    @Path("getDecryptedPassword")
	@Produces("text/plain")
	@RequiredRight("KPMG_DevOps_HelperBot_Decrypt_PW_SPRight")
	public String getDecryptedPW(@QueryParam("password") String encryptedPW) throws GeneralException, JSONException {
		log.debug("Encrypted PW::"+encryptedPW);
		SailPointContext context = getContext();
		log.debug("context::"+context);
		log.debug("Decrypted PW::"+context.decrypt(encryptedPW));
		return context.decrypt(encryptedPW);
	}

	@Override
	public String getPluginName() {
		return "KPMG_DevOps_Helper_Bot_Plugin";
	}
	
	private SailPointContext getcontext() throws GeneralException {
		return SailPointFactory.getCurrentContext();
	}
}